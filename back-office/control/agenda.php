<?php
      class agenda extends simplePHP {

        private $model;
        private $html;
        private $core;
        private $ui;
        private $util;
        private $file;

        public function __construct() {
    		    global $keys;

            #load model module
            $this->model = $this->loadModule('model');
            $this->model->context = true;

            #load html module
            $this->html = $this->loadModule('html');

            #load ui module
            $this->ui = $this->loadModule('ui');

            #load file module
            $this->file = $this->loadModule('file');

            #load util module
            $this->util = $this->loadModule('util');

            #load core module
            $this->core = $this->loadModule('core','',true);

            #footer
            $this->keys['footer'] = $this->includeHTML('../view/admin/footer.html');

            #topheader
            $this->keys['topheader'] =  $this->includeHTML('../view/admin/topheader.html');
            $this->keys['header'] =  $this->includeHTML('../view/admin/header.html');
            $this->keys['topo'] =  $this->includeHTML('../view/admin/topo.html');

            #menu
            $this->keys['menu'] =  $this->includeHTML('../view/admin/menu.html');
            $this->keys['sidemenu'] =  $this->includeHTML('../view/admin/sidemenu.html');
            $this->keys['topmenu'] =  $this->includeHTML('../view/admin/topmenu.html');

            $this->keys['pageTitle'] = "Agenda";

            $usuario = $this->model->getOne('usuario',$_SESSION['usuario_id']);
            $this->keys['usernameMaster'] = $_SESSION['usuario'];
            $this->keys['cliente_menu'] = $this->core->loadMenu();

            if ($_SESSION['tipo'] == "master") {
              $this->keys['activeestabelecimentos'] = 'active';
              $this->keys['admin'] = '';
              $this->keys['estabelecimento'] = 'hidden';
            } else{
              $this->keys['activeagenda'] = 'active';
              $this->keys['admin'] = 'hidden';
              $this->keys['estabelecimento'] = '';
            }
        }

        public function _actionStart() {
          $this->redirect('/agenda/listar');

          return $this->keys;
        }

        public function _actionListar() {
          $dado_id = $this->getParameter('3');

          if($dado_id == "") {
            $dado_id = $_SESSION['usuario_id'];
          }

          //consulta os agendamentos cadastrados no banco
          $consulta = $this->model->getData('agendamentos','a.id, a.data_agendamento, cli.nome', array('a.status' => 'Ativo', 'prof.estabelecimento_id' => $dado_id), "", "a.ID DESC", "INNER JOIN profissionais AS prof ON prof.id = a.profissional_id INNER JOIN clientes AS cli ON cli.id = a.cliente_id");

          if($consulta[0]['result'] != "empty") {
            for($i = 0; $i < count($consulta); $i++){
              $dados[$i]['id'] = $dados[$i]['url'] = $consulta[$i]['id'];
              $dados[$i]['title'] = $consulta[$i]['nome'];
              $dados[$i]['start'] = $consulta[$i]['data_agendamento'];
              
            }

            $this->keys['dados'] = json_encode($dados);
          } else{
            $this->keys['dados'] = "[{}]";
          }

          if($_SESSION['tipo'] == "estabelecimento") {
            $this->keys['visivel_insert'] = "";
            $this->keys['visivel_back'] = "hidden";
          } else{
            $this->keys['visivel_insert'] = "hidden";
            $this->keys['visivel_back'] = "";
          }

          $this->keys['id'] = $dado_id;

          return $this->keys;
        }
        public function _actionInserir() {
          $profissionais = $this->model->getList('profissionais', 'id', 'nome', array('estabelecimento_id' => $_SESSION['usuario_id']));

          $this->keys['select_profissionais'] = $this->html->select(false, $profissionais, 'profissionais' ,'', 0);

          $clientes = $this->model->getList('clientes', 'id', 'nome');

          $this->keys['select_clientes'] = $this->html->select(false, $clientes, 'clientes' ,'', 0);

          return $this->keys;
        }
        public function _actionGrava() {
          $agendamento = $_POST;
          $agendamento['data_agendamento'] = date_format(date_create($_POST['data_agendamento']), "Y-d-m") . " " . $_POST['hora_agendamento'];
          $dado_id = $_POST['id'];
          unset($agendamento['hora_agendamento']);

          $add = $this->model->addData('agendamentos',$agendamento,true);

          if (is_string($add)){
            die('sucesso;');
          } else{
            die('erro;');
          }
        }

        public function _actionAltera() {
          $agendamento = $_POST;
          $agendamento['data_agendamento'] = date_format(date_create($_POST['data_agendamento']), "Y-d-m") . " " . $_POST['hora_agendamento'];
          $dado_id = $_POST['id'];
          unset($agendamento['id'], $agendamento['hora_agendamento']);

          $this->model->alterData('agendamentos',$agendamento,array('id' => $dado_id));
          die('sucesso;');
        }

        public function _actionVer() {
          $dado_id = $this->getParameter('3');
          $this->keys += $this->model->getOne('agendamentos',$dado_id);

          $dataHora = explode(' ', $this->keys['data_agendamento']);
          $this->keys['data'] = date_format(date_create($dataHora[0]), "d/m/Y");
          $this->keys['hora'] = $dataHora[1];

          #imagem
          if($this->keys['avatar'] != '') {
            $this->keys['avatar'] = '/images/'.$this->keys['avatar'];
          } else {
            $this->keys['avatar'] = 'http://via.placeholder.com/200x200/';
          }

          $status = array("Ativo" => "Ativo", "Cancelado" => "Cancelado");
          $this->keys['select_status'] = $this->html->select(false, $status, 'status',$this->keys['status'],0);
          
          $this->keys['estabelecimento_id'] = $this->model->getOne("profissionais", $this->keys['profissional_id'])['estabelecimento_id'];

          $profissionais = $this->model->getList('profissionais', 'id', 'nome', array('estabelecimento_id' => $this->keys['estabelecimento_id']));

          $this->keys['select_profissionais'] = $this->html->select(false, $profissionais, 'profissionais' ,$this->keys['profissional_id']);

          $clientes = $this->model->getList('clientes', 'id', 'nome');

          $this->keys['select_clientes'] = $this->html->select(false, $clientes, 'clientes' ,$this->keys['cliente_id']);

          if ($_SESSION['tipo'] == 'master'){
            $this->keys['hidden'] = 'hidden';
          } else{
            $this->keys['hidden'] = '';
          }
          
          return $this->keys;
        }

        public function _actionFiltrar() {
          $modulo = $this->getParameter('1');

          foreach ($_POST as $key => $valueTxt) {
            $key = str_replace('like_','like ',$key);
            if($valueTxt != '') {
              $_SESSION['filtros'][$modulo][$key] = $valueTxt;
            }
            if($valueTxt == '') {
              unset($_SESSION['filtros'][$modulo][$key]);
            }

            if($_SESSION['filtros'][$modulo][$key] == '0') {
              unset($_SESSION['filtros'][$modulo][$key]);
            }
          }

          $this->redirect("/agenda/listar");
        }

        public function _actionLimpafiltros() {
          $modulo = $this->getParameter('1');
          unset($_SESSION['filtros'][$modulo]);
          $this->redirect("/agenda/listar");
        }
      }

?>

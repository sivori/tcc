<?php
      class meu_estabelecimento extends simplePHP {

        private $model;
        private $html;
        private $core;
        private $ui;
        private $util;
        private $file;

        public function __construct() {
    		    global $keys;

            #load model module
            $this->model = $this->loadModule('model');
            $this->model->context = true;

            #load html module
            $this->html = $this->loadModule('html');

            #load ui module
            $this->ui = $this->loadModule('ui');

            #load file module
            $this->file = $this->loadModule('file');

            #load util module
            $this->util = $this->loadModule('util');

            #load core module
            $this->core = $this->loadModule('core','',true);

            #footer
            $this->keys['footer'] = $this->includeHTML('../view/admin/footer.html');

            #topheader
            $this->keys['topheader'] =  $this->includeHTML('../view/admin/topheader.html');
            $this->keys['header'] =  $this->includeHTML('../view/admin/header.html');
            $this->keys['topo'] =  $this->includeHTML('../view/admin/topo.html');

            #menu
            $this->keys['menu'] =  $this->includeHTML('../view/admin/menu.html');
            $this->keys['sidemenu'] =  $this->includeHTML('../view/admin/sidemenu.html');
            $this->keys['topmenu'] =  $this->includeHTML('../view/admin/topmenu.html');

            $this->keys['pageTitle'] = "Meu Estabelecimeneto";

            $usuario = $this->model->getOne('usuario',$_SESSION['usuario_id']);
            $this->keys['usernameMaster'] = $_SESSION['usuario'];
            $this->keys['cliente_menu'] = $this->core->loadMenu();

            $this->keys['activemeu_estabelecimento'] = 'active';

        }

        public function _actionStart() {
          $this->redirect('/meu_estabelecimento/ver/'.$_SESSION['usuario_id']);
          return $this->keys;
        }
        public function _actionListar() {


              #dados do status
              if(strtolower($_SESSION['filtros']['']['status']) != '') {
                  if(strtolower($_SESSION['filtros']['']['status']) == 'ativo') {
                    $this->keys['statusAtivo'] = "selected";
                    $this->keys['statusInativo'] = "";
                  } else {
                    $this->keys['statusAtivo'] = "";
                    $this->keys['statusInativo'] = "selected";
                  }
              }
              #dados do estado
            $this->keys['select_estado'] = $this->html->select(false, $this->util->ufs(), 'estado','');



          $steper = 15;
          $modulo = $this->getParameter('1');
          $page = ($this->getParameter('3') != '') ? $this->getParameter('3') : 1;

          $total = $this->model->countData('estabelecimentos',$_SESSION['filtros']['estabelecimentos']);

          $this->keys['paginacao'] = $this->ui->pager($steper,$total,$page,'goUrl');

          $limits['limit'] = $steper;
          $limits['start'] = $this->calculaStartPaginacao($page,$steper);

          $dados = $this->model->getData('estabelecimentos','*',$_SESSION['filtros']['estabelecimentos'],$limits);

          if($_SESSION['filtros']['estabelecimentos'] != '') {
            $this->keys['limpar'] = '<a href="/estabelecimentos/limpafiltros" class="btn btn-info"><i class="glyphicon glyphicon-zoom-out" aria-hidden="true"></i> Limpar</a>';
            $this->keys['filtroativo'] = 'filtroativo';
          } else {
            $this->keys['limpar'] = '';
            $this->keys['filtroativo'] = '';
          }

          #aplica filtros
          foreach($_SESSION['filtros'][$modulo] as $key => $value) {
            $key = str_replace('like','',$key);
            $this->keys['filtro_'.trim($key)] = $value;
          }
          return $this->keys;
        }
        public function _actionInserir() {
          #imagem
                $this->keys['avatar'] = 'http://via.placeholder.com/200x200/';
              $this->keys['select_estado'] = $this->html->select(false, $this->util->ufs(), 'estado','');
          return $this->keys;
        }
        public function _actionGrava() {
          foreach ($_FILES as $key => $file) {
            if($file['tmp_name'] != '') {
              $_POST[$key] = $this->file->uploadFile($file,'images/');
            }
          }

          $_POST['senha'] = md5($_POST['senha']);$_POST['confirmar_senha'] = md5($_POST['confirmar_senha']);

          $_POST['usuario_id'] = $_SESSION['usuario_id'];

          $this->model->addData('estabelecimentos',$_POST,true);

          $usuario_estabelecimento['nome'] = $_POST['nome'];
          $usuario_estabelecimento['email'] = $_POST['email'];
          $usuario_estabelecimento['senha'] = $_POST['senha'];
          $usuario_estabelecimento['permissoes'] = '1, 10, 11,';
          $usuario_estabelecimento['tipo'] = 'estabelecimento';

          $this->model->addData('usuario',$usuario_estabelecimento,true);
          die('sucesso;');
        }

        public function _actionAltera() {
          $dado_id = $_REQUEST['id'];

          foreach ($_FILES as $key => $file) {
            if($file['tmp_name'] != '') {
              $_POST[$key] = '/images/'. $this->file->uploadFile($file,'images/');
            }
          }
    
          if ($_POST['senha'] != "") {
            $_POST['senha'] = md5(trim($_POST['senha']));
          } else{
            unset($_POST['senha']);
          }
    
          foreach ($_POST['dias_funcionamento'] as $value) {
            $diasFuncionamento .= $value . ",";
          }
    
          $_POST['dias_funcionamento'] = substr($diasFuncionamento, 0, -1);
    
          $_POST['valor'] = str_replace(",", ".", str_replace(".", "", $_POST['valor']));
          
          $alter = $this->model->alterData('estabelecimentos',$_POST,array('id' => $dado_id));
    
          if (is_bool($alter)){
            die('sucesso;');
          } else{
            die('erro;');
          }
        }

        public function _actionVer() {
          $dado_id = $this->getParameter('3');
          $this->keys += $this->model->getOne('estabelecimentos',$dado_id);
    
          #imagem
          if($this->keys['avatar'] != '') {
            $this->keys['avatar'] = $this->keys['avatar'];
          } else {
            $this->keys['avatar'] = 'http://via.placeholder.com/200x200/';
          }
    
          $this->keys['valor'] = number_format($this->keys['valor'], 2, ",", ".");
    
          $this->keys['select_estado'] = $this->html->select(false, $this->util->ufs(), 'estado', $this->keys['estado']);
    
          $status = array("Ativo" => "Ativo", "Inativo" => "Inativo");
          $this->keys['select_status'] = $this->html->select(false, $status, 'status', $this->keys['status'], 1, "Selecione");
    
          $diasFuncionamento = array("segunda" => "segunda", "terca" => "terca", "quarta" => "quarta", "quinta" => "quinta", "sexta" => "sexta", "sabado" => "sabado", "domingo" => "domingo");
          foreach ($diasFuncionamento as $dias) {
            if (strpos($this->keys['dias_funcionamento'], $dias) !== false) {
              $this->keys['check_' . $dias] = "checked";
            } else{
              $this->keys['check_' . $dias] = "";
            }
          }

          $horarios = array("00:00" => "00:00", "01:00" => "01:00", "02:00" => "02:00", "03:00" => "03:00", "04:00" => "04:00", "05:00" => "05:00", "06:00" => "06:00", "07:00" => "07:00", "08:00" => "08:00", "09:00" => "09:00", "10:00" => "10:00", "11:00" => "11:00", "12:00" => "12:00", "13:00" => "13:00", "14:00" => "14:00", "15:00" => "15:00", "16:00" => "16:00", "17:00" => "17:00", "18:00" => "18:00", "19:00" => "19:00", "20:00" => "20:00", "21:00" => "21:00", "22:00" => "22:00", "23:00" => "23:00");
          $this->keys['select_inicio_funcionamento'] = $this->html->select(false, $horarios, 'inicio_funcionamento', $this->keys['inicio_funcionamento'], 1, "Selecione");

          $this->keys['select_fim_funcionamento'] = $this->html->select(false, $horarios, 'fim_funcionamento', $this->keys['fim_funcionamento'], 1, "Selecione");

          return $this->keys;
        }

        public function _actionFiltrar() {
          $modulo = $this->getParameter('1');

          foreach ($_POST as $key => $valueTxt) {
            $key = str_replace('like_','like ',$key);
            if($valueTxt != '') {
              $_SESSION['filtros'][$modulo][$key] = $valueTxt;
            }
            if($valueTxt == '') {
              unset($_SESSION['filtros'][$modulo][$key]);
            }

            if($_SESSION['filtros'][$modulo][$key] == '0') {
              unset($_SESSION['filtros'][$modulo][$key]);
            }
          }

          $this->redirect("/estabelecimentos/listar");
        }

        public function _actionLimpafiltros() {
          $modulo = $this->getParameter('1');
          unset($_SESSION['filtros'][$modulo]);
          $this->redirect("/estabelecimentos/listar");
        }
      }
?>

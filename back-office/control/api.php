<?php
/**
* Project: Hinchliffe
* API
* @author Gabriel de Oliveira Lima <gabriel.leli@hotmail.com>
*/

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include "../modules/aws-autoloader.php";
use Aws\Sns\SnsClient;

class api extends simplePHP {

    #initialize vars
    private $model;
    private $util;
    private $core;
    private $xml;
    private $file;
    private $arn_base;
    private $image;
    private $arn_app;
    private $mp;

    /**
    *   Construtor
    **/
    public function __construct() {
        global $keys;

        #load model module
        $this->model = $this->loadModule("model");
        $this->model->context = false;

        #load core module
        $this->core = $this->loadModule("core","",true);
        $this->xml = $this->loadModule("xml");
        $this->file = $this->loadModule("file");
        $this->email = $this->loadModule("email");
        #load module for validations
        $this->validator = $this->loadModule("validator", "");
        #load image module
        $this->util = $this->loadModule('util');
        $this->image = $this->loadModule('image');


        $_SESSION["conta_id"] = 1;

        unset($_REQUEST["PHPSESSID"]);
    }

    public function _actionStart() {
      return $this->keys;
    }

    public function apiReturn($status, $mensagem = "", $data = "") {

        $retorno["status"] = $status;
        $retorno["mensagem"] = $mensagem;
        $retorno["data"] = $data;

        echo json_encode($retorno);
        exit;
    }

    /**
    * ATENÇÃO - Funcões destinado ao APP
    **/

    public function _actionLogin() {
        $data["email"] = trim($_REQUEST["email"]);
        $data["senha"] = md5(trim($_REQUEST["senha"]));

        $cliente = $this->model->getData("clientes", "id, nome, email", $data);

        if ($cliente[0]["result"] != "empty") {
          unset($cliente[0]['senha']);
          $this->apiReturn("success","Login realizado com sucesso!", $cliente[0]);
        } else {
          $this->apiReturn("error","Não foi possivel realizar o login, verifique o usuário e a senha.");
        }

    }

  public function _actionBuscaEstabelecimentos(){
    $id = $_REQUEST['id'];
    $estabelecimentos = $this->model->getData('estabelecimentos', '*');

    if ($estabelecimentos[0]['result'] != "empty") {
      $hora = date("H:i");
      $semana = date("w");
      $arrSemana = array(0 => "domingo", 1 => "segunda", 2 => "terca", 3 => "quarta", 4 => "quinta", 5 => "sexta", 6 => "sabado");

      foreach ($estabelecimentos as $key => $value) {
        if (strstr($value['dias_funcionamento'], $arrSemana[$semana])) {
          $favoritos = $this->model->getData('favoritos', '*', array('cliente_id' => $id, 'estabelecimento_id' => $value['id']));
          
          if ($favoritos[0]['result'] != "empty") {
            $estabelecimentos[$key]['favoritos'] = '1';
          } else {
            $estabelecimentos[$key]['favoritos'] = '0';
          }

          $estabelecimentos[$key]['valor'] = number_format($value['valor'], 2, ",", ".");

          if ($hora < $value['inicio_funcionamento'] || $hora > $value['fim_funcionamento']) {
            unset($estabelecimentos[$key]);
          }
        } else{
          unset($estabelecimentos[$key]);
        }
      }

      unset($estabelecimentos[0]['permissoes'], $estabelecimentos[0]['time'], $estabelecimentos[0]['usuario_id'], $estabelecimentos[0]['conta_id'], $estabelecimentos[0]['ip']);
      $this->apiReturn('sucesso','', $estabelecimentos);
     } else {
      $this->apiReturn('error','Não encontramos nenhum estabelecimento. Tente novamente mais tarde.');
    }
  }

   public function _actionBuscaEstabelecimentosFavoritos(){
    $id = $_REQUEST['id'];
    $estabelecimentos = $this->model->getData('estabelecimentos', '*', array('f.cliente_id' => $id), '', 'a.ID DESC', 'JOIN favoritos as f ON a.id = f.estabelecimento_id');

    if ($estabelecimentos[0]['result'] != "empty") {
      foreach ($estabelecimentos as $key => $value) {
        $estabelecimentos[$key]['favoritos'] = '1';
      }

      $this->apiReturn('sucesso','', $estabelecimentos);
    } else {
      $this->apiReturn('error','Não encontramos nenhum estabelecimento. Tente novamente mais tarde.');
    }
  }

  public function _actionCadastraFavorito(){
    $favorito_id = $this->model->addData("favoritos", $_REQUEST, true);

    if (isset($favorito_id)) {
      $this->apiReturn('sucesso','', $favorito_id);
    }else {
      $this->apiReturn('error','Não encontramos nenhum estabelecimento. Tente novamente mais tarde.');
    }
  }

  public function _actionRemoveFavorito(){
    $this->model->removeData("favoritos", $_REQUEST);

    $this->apiReturn('sucesso','', '');
  }

  public function _actionAlteraPerfil(){
      $data = $_REQUEST;
      unset($data['id'], $data['confirma']);

      if (isset($data['email'])){
        $data["email"] = trim($_REQUEST["email"]);
      }

      if (isset($data['senha'])) {
        $data["senha"] = md5(trim($_REQUEST["senha"]));
      } else {
        unset($data['senha']);
      }

      $cliente_id = $this->model->alterData("clientes", $data, array('id' => $_REQUEST['id']));

      if (is_bool($cliente_id)) {
        $dados = $this->model->getOne("clientes", $_REQUEST['id']);
        unset($dados['senha']);
        $this->apiReturn("success","Perfil alterado com sucesso.", $dados);
      } else {
        $this->apiReturn("error","Não foi possível concluir o cadastro. Tente novamente.");
      }
  }

    public function _actionCadastro(){
      $data = $_REQUEST;
      $data["email"] = trim($_REQUEST["email"]);
      $data["status"] = 'Ativo';

      $res = $this->model->getData("clientes", "a.id", array("email" => $data["email"]));

      if ($res[0]["result"] != "empty") {
        $this->apiReturn("error","E-mail já cadastrado, tente novamente com outro e-mail ou faça login.");
      }

      $data["senha"] = md5(trim($_REQUEST["senha"]));
      unset($data["confirma"]);

      $cliente_id = $this->model->addData("clientes", $data, true);

      if (is_string($cliente_id)) {
        $data["id"] = $cliente_id;
        $this->apiReturn("success","Cadastro realizado com sucesso.", $data);
      } else {
        $this->apiReturn("error","Não foi possível concluir o cadastro. Tente novamente.");
      }
    }

    public function _actionEsqueciSenha() {

        $data = $_REQUEST;

        $cliente = $this->model->getData('clientes','a.id, a.email, a.nome', array('email'=>$data['email']));

        if ($cliente[0]['result'] == 'empty') {
            $this->apiReturn('error','O e-mail informado não está cadastrado.');
        } else {

            for($i=0; $i<5; $i++){
                $code .= "".mt_rand(0, 9);
            }

            $nome = explode(' ', $cliente[0]['nome']);
            $senha = substr(md5(rand(0,10000)),0,6);
            $content = array("nome"=>$nome[0], "senha"=>$senha, "data"=>Date("d/m/Y h:i"));
            $template = $this->includeHTML("../view/emails/esqueci_senha.html");

            $this->model->alterData('clientes',array('senha'=>md5($senha)),array('email'=>$data['email']));

            $enviou = $this->email->send($data['email'], CONTACT_EMAIL, utf8_encode("[Modena] Recuperação de senha"), $template, $content, "Modena");

            if ($enviou == 1) {

                $recovery = array('data_recovery' => date('Y-m-d h:i:s'), 'cliente_id' => $cliente[0]['id'], 'code' => $code, 'status' => 'Ativo');
                $add = $this->model->addData('recovery_pass', $recovery, true);

                $this->apiReturn('success','E-mail enviado com sucesso', $code);
            } else {
                $this->apiReturn('error','Ocorreu um erro ao enviar seu e-mail. Tente novamente.');
            }
        }
    }


    public function _actionAtualizarCliente(){

        $data = $_REQUEST;
        if($_REQUEST['senha'] != '') {
          $data['senha'] = md5(trim($_REQUEST['senha']));
        }

        $res = $this->model->alterData('clientes', $data, array('id' => $_REQUEST['id']));

        if ($res['status'] == 'erro') {
            $this->apiReturn('error','Não foi possível atualizar dos dados. Tente novamente.');
        } else {
            unset($_REQUEST['senha'], $_REQUEST['senha_atual']);
            $this->apiReturn('success','Dados atualizados com sucesso', $_REQUEST);
        }
    }

    public function _actionRedefinirSenha() {

        $data = $_REQUEST;
        $senha = md5(trim($data['senha']));

        $res = $this->model->alterData('clientes', array('senha'=>$senha), array('id' => $data['id']));

        if ($res['status'] == 'erro') {
            $this->apiReturn('error','Não foi possível atualizar dos dados. Solicite novamente.');
        } else {
            $this->apiReturn('success','Dados atualizados com sucesso');
        }

    }
    public function _actionEmpreendimentos() {
      $res = $this->model->getData('empreendimentos','id,nome,metragens,dormitorios,slogan,conceito,status,latitude,longitude,situacao,cep,endereco,numero,bairro,cidade,estado',array('status'=>'Ativo'));
      foreach ($res as $empreendimento) {
        $empreendimento['multimidia'] = $this->model->getData('multimidia','conceito,galeria,destaque,tipo,url,legenda,foto,categoria',array('empreendimento_id'=>$empreendimento['id']));

      $empreendimento['caracteristicas'] = $this->model->getData('caracteristicas','nome,valor',array('empreendimento_id'=>$empreendimento['id']));
       $empreendimento['conceito'] = nl2br($empreendimento['conceito']);

        $empreendimentos[$empreendimento['id']] = $empreendimento;
      }
      $this->apiReturn('success','',$empreendimentos);
     return $this->keys;
   }

   public function _actionEntregas() {

     $res = $this->model->getData('entregas','a.*,e.nome',array('empreendimento_id'=>$_REQUEST['empreendimento_id']),'','a.id desc','inner join empreendimentos as e on e.id = a.empreendimento_id');

    foreach ($res as $key => $entrega) {
      $entrega['imagens'] = $this->model->getData('imagens_entrega','id,arquivo',array('entrega_id'=>$entrega['id']));
      $entregas[] = $entrega;
    }

    $this->apiReturn('success','',$entregas);
   }

   public function _actionMensagem() {
      $chat = $this->model->getData('chats','id',array('cliente_id'=>$_REQUEST['cliente_id']));
      if($chat[0]['result'] == 'empty') {
        $data = $_POST;
        $data['ultima_mensagem'] = $_POST['mensagem'];
        unset($data['mensagem']);
        $data['data_e_hora'] = date('d/m/Y H:i');

        $chat_id = $this->model->addData('chats',$data,true);
      } else {
        $chat_id = $chat[0]['id'];
      }
      $mensagem = $_POST;
      $mensagem['texto'] = $_POST['mensagem'];
      $mensagem['chat_id'] = $chat_id;
      unset($mensagem['mensagem']);
      unset($mensagem['ultima_mensagem']);
      $this->model->addData('mensagens',$mensagem,true);

      return $this->keys;
  }

  public function _actionChat() {
    $chat = $this->model->getData('chats','id',array('cliente_id'=>$_REQUEST['cliente_id']));
    if($chat[0]['result'] == 'empty') {
      $data = $_POST;
      $data['ultima_mensagem'] = $_POST['mensagem'];
      unset($data['mensagem']);
      $data['data_e_hora'] = date('d/m/Y H:i');

      $chat[0]['id'] = $this->model->addData('chats',$data,true);
    }
    $res = $this->model->getData('mensagens','texto,UNIX_TIMESTAMP(time) as time,autor',array('chat_id'=>$chat[0]['id']),'','a.id asc');
    foreach($res as $msg) {
      $msg['data'] = date('d/m/Y H:i',$msg['time']);
      $mensagens[] = $msg;
    }
    $this->apiReturn('sucess','',$mensagens);
 }

 public function _actionGravaaparelho() {
      $client = SnsClient::factory(array(
          "region" => "us-east-1",
          "key" => "AKIAI5C3ZUUWXZM5YYHQ",
          "secret" => "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"
      ));


      if(strlen($_REQUEST["aparelho"]) == 64) {
        // $response = $client->CreatePlatformEndpoint(array(
        //   "PlatformApplicationArn"=> PUSH_URL."app/APNS".APNS,
        //   "Token"=> $_REQUEST["aparelho"]));

        $response = $client->CreatePlatformEndpoint(array(
          "PlatformApplicationArn"=> PUSH_URL."app/APNS/".APNS,
          "Token"=> $_REQUEST["aparelho"]));
      }else{
        $response = $client->CreatePlatformEndpoint(array(
          "PlatformApplicationArn"=> PUSH_URL."app/GCM/".APNS."GCM",
          "Token"=> $_REQUEST["aparelho"]));
      }

      $endpoint = $response["EndpointArn"];

      $this->core->subscribe(APNS,$endpoint);

      $this->model->alterData("clientes",array("aparelho"=>$_REQUEST["aparelho"],"endpoint"=>$endpoint),array("id"=>$_REQUEST["cliente_id"]));


      $retorno["status"] = "sucesso";

      echo json_encode($retorno);
      exit;
    }

    public function _actionPoliview() {

     echo file_get_contents($_REQUEST['url']);
     exit;
    }

    public function _actionBuscaHorariosAgendados() {
      $_REQUEST['profissional'] = (array) json_decode($_REQUEST['profissional']);
      $_REQUEST['estabelecimento'] = (array) json_decode($_REQUEST['estabelecimento']);

      $weeks = array("domingo", "segunda", "terca", "quarta", "quinta", "sexta", "sabado");
      $horarios = array();
      
      $dataInicio = date("Y-m-d H:i");
      $dataFim = date("Y-m-d H:i", mktime (23, 59, 0, date("m"),  date("d"),  date("Y")));

      $dataSplit = explode(" ", $dataInicio);
      $hora = explode(":", $dataSplit[1])[0];

      $count = 0;
      $i = 0;

      while($count < 5) {
        $semana = date("w", strtotime($i . " day", strtotime(date("Y-m-d"))));
        
        if(strpos($_REQUEST['estabelecimento']['dias_funcionamento'], $weeks[$semana])) {
          $day = date("d/m/Y", strtotime($i . " day", strtotime(date("Y-m-d"))));
          $horarios[$count]["data"] = $day;

          $count1 = 0;

          $horaInicio = explode(":", $_REQUEST['estabelecimento']['inicio_funcionamento'])[0];
          $horaFim = explode(":", $_REQUEST['estabelecimento']['fim_funcionamento'])[0];

          for($j = $horaInicio + 1; $j < $horaFim; $j++) {
            if($day == date("d/m/Y")) {
              if($j > date("H")) {
                $horarios[$count]['horarios'][] = $j . ":00";
              }
            } else{
              $horarios[$count]['horarios'][] = $j . ":00";
            }

            $count1++;
          }

          $count++;
        }
        $i++;
      }

      $filter['profissional_id'] = $_REQUEST['profissional']['id'];
      $filter['>=data_agendamento'] = "'" . $dataInicio . "'";
      $filter['<=data_agendamento'] = "'" . $dataFim . "'";
      $filter['status'] = "Ativo";

      $agendamentos = $this->model->getData("agendamentos", "data_agendamento", $filter);

      if($agendamentos[0]['result'] != "empty") {
        foreach($agendamentos as $value) {
          $dataValue = date("d/m/Y", strtotime(explode(" ", $value['data_agendamento'])[0]));
          $horaValue = explode(" ", $value['data_agendamento'])[1];

          for($i = 0; $i < 5; $i++) {
            if($horarios[$i]['data'] == $dataValue) {
              $keyHorarios = array_search($horaValue, $horarios[$i]['horarios']);

              if(is_numeric($keyHorarios)) {
                unset($horarios[$i]['horarios'][$keyHorarios]);
              }
            }
          }
        }
      }

      $this->apiReturn('sucesso','', $horarios);
    }

    public function _actionConfirmarAgendamento() {
      $_REQUEST['data'] = explode(" - ", $_REQUEST['data']);
      $data = explode("/", $_REQUEST['data'][0]);

      $filter['cliente_id'] = $_REQUEST['usuarioId'];
      $filter['profissional_id'] = $_REQUEST['profissionalId'];
      $filter['data_agendamento'] = $data[2] . "-" . $data[1] . "-" . $data[0] . " " . $_REQUEST['data'][1];
      $filter['status'] = "Ativo";

      $add = $this->model->addData('agendamentos',$filter,true);

      if (is_string($add)){
        $this->apiReturn('sucesso','O Agendamento foi confirmado.');
      } else{
        $this->apiReturn('error','Não foi possivel confirmar o agendamento.');
      }
    }

    public function _actionBuscarProfissionais() {
      $filter['estabelecimento_id'] = $_REQUEST['estabelecimento_id'];
      $filter['status'] = "Ativo";

      $profissionais = $this->model->getData('profissionais', 'id, nome', $filter);

      if($profissionais[0]['result'] != "empty") {
        $this->apiReturn('sucesso','', $profissionais);
      } else {
        $this->apiReturn('error','Não encontramos nenhum horario agendado.');
      }
    }
}

?>
<?php
/**
* Project: Hinchliffe
*
* @author Gabriel de Oliveira Lima <gabriel.leli@hotmail.com>
*/

class relatorio extends simplePHP {

    #initialize vars
    private $model;
    private $html;
    private $core;
    private $ui;

    public function __construct() {
        global $keys;

        #load model module
        $this->model = $this->loadModule('model');
        $this->model->context = true;

        #load html module
        $this->html = $this->loadModule('html');

        #load ui module
        $this->ui = $this->loadModule('ui');

        #load file module
        $this->file = $this->loadModule('file');

        #load util module
        $this->util = $this->loadModule('util');

        #load core module
        $this->core = $this->loadModule('core','',true);

        #footer
        $this->keys['footer'] = $this->includeHTML('../view/admin/footer.html');

        #topheader
        $this->keys['topheader'] =  $this->includeHTML('../view/admin/topheader.html');
        $this->keys['header'] =  $this->includeHTML('../view/admin/header.html');
        $this->keys['topo'] =  $this->includeHTML('../view/admin/topo.html');

        #menu
        $this->keys['menu'] =  $this->includeHTML('../view/admin/menu.html');
        $this->keys['sidemenu'] =  $this->includeHTML('../view/admin/sidemenu.html');
        $this->keys['topmenu'] =  $this->includeHTML('../view/admin/topmenu.html');

        $this->keys['pageTitle'] = 'Relatórios';

        $usuario = $this->model->getOne('usuario',$_SESSION['usuario_id']);
        $this->keys['usernameMaster'] = $_SESSION['usuario'];
        $this->keys['cliente_menu'] = $this->core->loadMenu();

        $this->keys['activerelatorio'] = 'active';



    }

    public function _actionStart() {

        return $this->keys;
    }

    public function _actionCorrida() {
        #define o numero de elementos
        $steper = 30;

        // #define a pagina atual
        $page = ($this->getParameter(3) == "") ? 1 : $this->getParameter(3);

        $limits["limit"] = $steper;
        $limits["start"] = $this->calculaStartPaginacao($page,$steper);

        /* FILTRA APENAS OS PEDIDOS CONCLUIDOS */
        // $filters["status"] = "Concluido";

        if ($this->getParameter(4)){
            $filters["like mt.nome"] = $this->getParameter(4);
            $this->keys["motorista_nome"] = $this->getParameter(4);
        } else {
            $this->keys["motorista_nome"] = "";
        }

        if ($this->getParameter(5)){
            $filters[">= a.inicio"] = $this->core->preparaDataParaConsulta($this->getParameter(5));
            $this->keys["data_inicial"] = str_replace("_","/", $this->getParameter(5));
        } else {
            $this->keys["data_inicial"] = "";
        }

        if ($this->getParameter(6)){
            $filters["<= a.fim"] = $this->core->preparaDataParaConsulta($this->getParameter(6));
            $this->keys["data_final"] = str_replace("_","/", $this->getParameter(6));
        } else {
            $this->keys["data_final"] = "";
        }

        if ($this->getParameter(7)){
           $this->keys["valor_min"] = $this->getParameter(7);
           $filters[">= valor"] = $this->core->removeMaskMoney($this->getParameter(7));
        } else {
           $this->keys["valor_min"] = "";
        }

        if ($this->getParameter(8)){
           $this->keys["valor_max"] = $this->getParameter(8);
           $filters["<= valor"] = $this->core->removeMaskMoney($this->getParameter(8));
        } else {
           $this->keys["valor_max"] = "";
        }

        /* QUERY PARA PAGINACAO */
        $data = $this->model->getData("pedido", "*, cl.nome as cliente_nome, mt.nome as motorista_nome, mt.razao_social as motorista_social", $filters, $limits,"a.id ASC", "inner join motorista as mt on a.motorista_id = mt.id inner join cliente as cl on a.cliente_id = cl.id");

        $valores = $this->model->getData("pedido","SUM(comissao_carlog + comissao_motorista) as total, SUM(comissao_motorista) as total_motorista", $filters,"","a.id ASC","inner join motorista as mt on a.motorista_id = mt.id inner join cliente as cl on a.cliente_id = cl.id");

        $total = $this->model->countData("pedido", $filters);

        $parametrosAdicionais = $this->getParameter(4)."/".$this->getParameter(5)."/".$this->getParameter(6)."/".$this->getParameter(7)."/".$this->getParameter(8);
        $this->keys["paginacao"] = $this->ui->pager($steper, $total, $page, "goUrl", $parametrosAdicionais);

        // Fim da paginação
        if($data[0]["result"] == "empty") {
            $this->keys["displayTotais"] = "hidden";
            $this->keys["table"] = $this->includeHTML("../view/helpers/vazio.html");
            $this->keys["paginacao"] = "";
            $this->keys["totalCorridas"] = reais(0);
            $this->keys["totalMotoristas"] = reais(0);
        } else {
            $lista[0][0] = "Data do pedido";
            $lista[0][1] = "Forma de pagamento";
            $lista[0][2] = "Cliente";
            $lista[0][3] = "Motorista";
            $lista[0][4] = "Valor Motorista";
            $lista[0][5] = "Valor Total";
            $lista[0][6] = "Hora de início";
            $lista[0][7] = "Hora de término";

            foreach ($data as $pedido) {
                $saida["data"] = $this->core->preparaData($pedido["inicio"]);
                $saida["forma_pagamento"] = "-";
                $saida["cliente_nome"] = $pedido["cliente_nome"];
                $saida["motorista_nome"] = ($pedido["motorista_nome"]) ? $pedido["motorista_nome"] : $pedido["motorista_social"];

                $saida["valor"] = reais($pedido["comissao_motorista"]);
                $saida["valor_total"] = reais($pedido["comissao_motorista"] + $pedido["comissao_carlog"]);
                $saida["inicio"]  = date("h:i", $pedido["inicio"]);
                $saida["final"]   = date("h:i", $pedido["fim"]);

                $lista[] = $saida;
            }

            $this->keys["totalCorridas"] = reais($valores[0]["total"]);
            $this->keys["totalMotoristas"] = reais($valores[0]["total_motorista"]);

            $this->keys["table"] = $this->html->table($lista,array("class"=>"table table-bordered table-hover table-striped upper text-center","id"=>"lista-corridas"),true,"","",true);
        }

        if($this->getParameter(9)){
            $exportData = $this->model->getData("pedido", "*, cl.nome as cliente_nome, mt.nome as motorista_nome, mt.razao_social as motorista_social", $filters, $limits,"a.id ASC", "inner join motorista as mt on a.motorista_id = mt.id inner join cliente as cl on a.cliente_id = cl.id");

            foreach ($exportData as $item) {
                $out["data"] = $this->core->preparaData($item["inicio"]);
                $out["forma_pagamento"] = "-";
                $out["cliente_nome"] = $item["cliente_nome"];
                $out["motorista_nome"] = ($item["motorista_nome"]) ? $item["motorista_nome"] : $item["motorista_social"];

                $out["valor"] = reais($item["comissao_motorista"]);
                $out["valor_total"] = reais($item["comissao_motorista"] + $item["comissao_carlog"]);
                $out["inicio"]  = date("h:i", $item["inicio"]);
                $out["final"]   = date("h:i", $item["fim"]);

                $content[] = $out;
            }
            $csv = $this->loadModule("csv");
            $csv->export($content, "corridas.csv");
            exit;
        }
        return $this->keys;
    }

    public function _actionMotorista() {
        #define o numero de elementos
        $steper = 30;

        // #define a pagina atual
        $page = ($this->getParameter(3) == "") ? 1 : $this->getParameter(3);

        $limits["limit"] = $steper;
        $limits["start"] = $this->calculaStartPaginacao($page, $steper);

        /* FILTRA APENAS OS PEDIDOS CONCLUIDOS */
        if ($this->getParameter(4)){
            $filters["like mt.nome"] = $this->getParameter(4);
            $this->keys["motorista_nome"] = $this->getParameter(4);
        } else {
            $this->keys["motorista_nome"] = "";
        }

        if ($this->getParameter(5)){
            $filters[">= a.inicio"] = $this->core->preparaDataParaConsulta($this->getParameter(5));
            $this->keys["data_inicial"] = str_replace("_","/", $this->getParameter(5));
        } else {
            $this->keys["data_inicial"] = "";
        }

        if ($this->getParameter(6)){
            $filters["<= a.fim"] = $this->core->preparaDataParaConsulta($this->getParameter(6));
            $this->keys["data_final"] = str_replace("_","/", $this->getParameter(6));
        } else {
            $this->keys["data_final"] = "";
        }

        /* QUERY PARA PAGINACAO */
        $data = $this->model->getData("pedido", "a.id, a.time, a.motorista_id, a.cliente_id, a.comissao_motorista, a.comissao_carlog, a.inicio, a.valor, mt.nome as motorista_nome, SUM(comissao_motorista) as valor_motorista, SUM(valor) as total, COUNT(a.id) as quantidade", $filters, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id","a.motorista_id");

        $valores = $this->model->getData("pedido","SUM(comissao_carlog + comissao_motorista) as total, SUM(comissao_motorista) as total_motorista", $filters,"","a.id desc","inner join motorista as mt on a.motorista_id = mt.id");

        // $filtersAceitas = array();
        // $filtersAceitas = $filters;
        // $filtersAceitas["a.status"] = "finalizada";
        // $aceitas = $this->model->getData("pedido","COUNT(a.id) as aceitas", $filtersAceitas, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join pedido_recusada as cr on a.id = cr.pedido_id");
        // $totAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;
        //
        // $filtersCanceladas = array();
        // $filtersCanceladas = $filters;
        // $filtersCanceladas["a.status"] = "cancelada";
        // $canceladas = $this->model->getData("pedido","COUNT(a.id) as canceladas", $filtersCanceladas,  $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join pedido_recusada as cr on a.id = cr.pedido_id");
        // $totRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

        $total = $this->model->countData("pedido", $filters);

        $parametrosAdicionais = $this->getParameter(4)."/".$this->getParameter(5)."/".$this->getParameter(6);
        $this->keys["paginacao"] = $this->ui->pager($steper, $total, $page, "goUrl", $parametrosAdicionais);

        // Fim da paginação
        if($data[0]["result"] == "empty") {
            $this->keys["displayTotais"] = "hidden";
            $this->keys["table"] = $this->includeHTML("../view/helpers/vazio.html");
            $this->keys["paginacao"] = "";
            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais(0);
            $this->keys["totalMotoristas"] = reais(0);
        } else {

            $lista[0][0] = "Motorista";
            $lista[0][1] = "Valor Motorista";
            $lista[0][2] = "Valor Total";
            $lista[0][3] = "Corridas";
            $lista[0][4] = "Chamadas aceitas";
            $lista[0][5] = "Chamadas recusadas";

            foreach ($data as $pedido) {

                $aceitas = $this->model->getData("pedido","COUNT(id) as aceitas",array("status"=>"finalizada","motorista_id"=>$pedido["motorista_id"]));
                $countAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

                $canceladas = $this->model->getData("pedido","COUNT(id) as canceladas",array("status"=>"cancelada","motorista_id"=>$pedido["motorista_id"]));
                $countRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

                $saida["motorista_nome"] = $pedido["motorista_nome"];
                $saida["valor_motorista"] = reais($pedido["valor_motorista"]);
                $saida["valor_total"] = reais($pedido["total"]);
                $saida["pedidos"]  = $pedido["quantidade"];
                $saida["aceitas"]   = $countAc;
                $saida["recusadas"]   = $countRec;

                $lista[] = $saida;
            }

            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais($valores[0]["total"]);
            $this->keys["totalMotoristas"] = reais($valores[0]["total_motorista"]);

            $this->keys["table"] = $this->html->table($lista, array("class"=>"table table-bordered table-hover table-striped upper text-center","id"=>"lista-corridas"),true,"","",true);
            $this->keys["paginacao"] = $this->ui->pager($steper, $total, $page, "goUrl");
        }

        if($this->getParameter(7)){
            $exportData = $this->model->getData("pedido", "a.id, a.time, a.motorista_id, a.cliente_id, a.comissao_motorista, a.comissao_carlog, a.inicio, a.valor, mt.nome as motorista_nome, SUM(comissao_motorista) as valor_motorista, SUM(valor) as total, COUNT(a.id) as quantidade", $filters, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id","a.motorista_id");

            foreach ($exportData as $item) {
                $aceitas = $this->model->getData("pedido","COUNT(id) as aceitas",array("status"=>"finalizada","motorista_id"=>$item["motorista_id"]));
                $countAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

                $canceladas = $this->model->getData("pedido","COUNT(id) as canceladas",array("status"=>"cancelada","motorista_id"=>$item["motorista_id"]));
                $countRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

                $out["motorista_nome"] = $item["motorista_nome"];
                $out["valor_motorista"] = reais($item["valor_motorista"]);
                $out["valor_total"] = reais($item["total"]);
                $out["pedidos"]  = $item["quantidade"];
                $out["aceitas"]   = $countAc;
                $out["recusadas"]   = $countRec;

                $content[] = $out;
            }
            $csv = $this->loadModule("csv");
            $csv->export($content, "motoristas.csv");
            exit;
        }
        return $this->keys;
    }

    public function _actionReceber() {
        #define o numero de elementos
        $steper = 30;

        // #define a pagina atual
        $page = ($this->getParameter(3) == "") ? 1 : $this->getParameter(3);

        $limits["limit"] = $steper;
        $limits["start"] = $this->calculaStartPaginacao($page, $steper);

        /* FILTRA APENAS OS PEDIDOS CONCLUIDOS */
        if ($this->getParameter(4)){
            $filters["like mt.nome"] = $this->getParameter(4);
            $this->keys["nome"] = $this->getParameter(4);
        } else {
            $this->keys["nome"] = "";
        }

        if ($this->getParameter(5)){
            $filters["like mt.nome"] = $this->getParameter(5);
            $this->keys["ordem"] = $this->getParameter(5);
        } else {
            $this->keys["ordem"] = "";
        }

        $status = $this->getParameter(6);
        if($status){
            $filters["a.status"] = $status;
        }
        $statusArray = array("ativo" => "Ativo", "inativo" => "Inativo");
        $this->keys["selectStatus"] = $this->html->select(false, $statusArray, "status", $status, "", "");

        if ($this->getParameter(7)){
            $filters[">= a.inicio"] = $this->core->preparaDataParaConsulta($this->getParameter(7));
            $this->keys["data_inicial"] = str_replace("_","/", $this->getParameter(7));
        } else {
            $this->keys["data_inicial"] = "";
        }

        if ($this->getParameter(8)){
            $filters["<= a.fim"] = $this->core->preparaDataParaConsulta($this->getParameter(8));
            $this->keys["data_final"] = str_replace("_","/", $this->getParameter(8));
        } else {
            $this->keys["data_final"] = "";
        }

        /* QUERY PARA PAGINACAO */
        $data = $this->model->getData("corrida", "a.id, a.time, a.motorista_id, a.cliente_id, a.comissao_motorista, a.comissao_carlog, a.inicio, a.fim, a.valor, mt.nome as motorista_nome, SUM(comissao_motorista) as valor_motorista, SUM(valor) as total, COUNT(a.id) as quantidade, COUNT(cr.id) as crecusadas", $filters, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id","a.motorista_id");

        $valores = $this->model->getData("corrida","SUM(comissao_carlog + comissao_motorista) as total, SUM(comissao_motorista) as total_motorista", $filters,"","a.id desc","inner join motorista as mt on a.motorista_id = mt.id");

        $filtersAceitas = array();
        $filtersAceitas = $filters;
        $filtersAceitas["a.status"] = "finalizada";
        $aceitas = $this->model->getData("corrida","COUNT(a.id) as aceitas", $filtersAceitas, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id");
        $totAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

        $filtersCanceladas = array();
        $filtersCanceladas = $filters;
        $filtersCanceladas["a.status"] = "cancelada";
        $canceladas = $this->model->getData("corrida","COUNT(a.id) as canceladas", $filtersCanceladas,  $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id");
        $totRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

        $total = $this->model->countData("corrida", $filters);

        $parametrosAdicionais = $this->getParameter(4)."/".$this->getParameter(5)."/".$this->getParameter(6)."/".$this->getParameter(7)."/".$this->getParameter(8);
        $this->keys["paginacao"] = $this->ui->pager($steper, $total, $page, "goUrl", $parametrosAdicionais);

        // Fim da paginação
        if($data[0]["result"] == "empty") {
            $this->keys["displayTotais"] = "hidden";
            $this->keys["table"] = $this->includeHTML("../view/helpers/vazio.html");
            $this->keys["paginacao"] = "";
            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais(0);
            $this->keys["totalMotoristas"] = reais(0);
        } else {

            $lista[0][0] = "Motorista";
            $lista[0][1] = "Valor Motorista";
            $lista[0][2] = "Valor Total";
            $lista[0][3] = "Corridas";
            $lista[0][4] = "Chamadas aceitas";
            $lista[0][5] = "Chamadas recusadas";

            foreach ($data as $corrida) {

                $aceitas = $this->model->getData("corrida","COUNT(id) as aceitas",array("status"=>"finalizada","motorista_id"=>$corrida["motorista_id"]));
                $countAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

                $canceladas = $this->model->getData("corrida","COUNT(id) as canceladas",array("status"=>"cancelada","motorista_id"=>$corrida["motorista_id"]));
                $countRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

                $saida["motorista_nome"] = $corrida["motorista_nome"];
                $saida["valor_motorista"] = reais($corrida["valor_motorista"]);
                $saida["valor_total"] = reais($corrida["total"]);
                $saida["corridas"]  = $corrida["quantidade"];
                $saida["aceitas"]   = $countAc;
                $saida["recusadas"]   = $countRec;

                $lista[] = $saida;
            }

            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais($valores[0]["total"]);
            $this->keys["totalMotoristas"] = reais($valores[0]["total_motorista"]);

            $this->keys["table"] = $this->html->table($lista,array("class"=>"table table-bordered table-hover table-striped upper text-center","id"=>"lista-corridas"),true,"","",true);
            $this->keys["paginacao"] = $this->ui->pager($steper,$total,$page,"goUrl");
        }
        return $this->keys;
    }

    public function _actionPagar() {
        #define o numero de elementos
        $steper = 30;

        // #define a pagina atual
        $page = ($this->getParameter(3) == "") ? 1 : $this->getParameter(3);

        $limits["limit"] = $steper;
        $limits["start"] = $this->calculaStartPaginacao($page, $steper);

        /* FILTRA APENAS OS PEDIDOS CONCLUIDOS */
        if ($this->getParameter(4)){
            $filters["like mt.nome"] = $this->getParameter(4);
            $this->keys["motorista_nome"] = $this->getParameter(4);
        } else {
            $this->keys["motorista_nome"] = "";
        }

        $tipo = $this->getParameter(5);
        if($tipo){
            $filters["a.tipo"] = $tipo;
        }
        $arrTipos = array("pf"=>"PF", "pj"=>"PJ", "loc"=>"Locadora");
        $this->keys["selectTipo"] = $this->html->select(false, $arrTipos, "tipo", $tipo, "", "");

        $status = $this->getParameter(6);
        if($status){
            $filters["a.status"] = $status;
        }
        $statusArray = array("ativo" => "Ativo", "inativo" => "Inativo");
        $this->keys["selectStatus"] = $this->html->select(false, $statusArray, "status", $status, "", "");

        if ($this->getParameter(7)){
            $filters[">= a.inicio"] = $this->core->preparaDataParaConsulta($this->getParameter(7));
            $this->keys["data_inicial"] = str_replace("_","/", $this->getParameter(7));
        } else {
            $this->keys["data_inicial"] = "";
        }

        if ($this->getParameter(8)){
            $filters["<= a.fim"] = $this->core->preparaDataParaConsulta($this->getParameter(8));
            $this->keys["data_final"] = str_replace("_","/", $this->getParameter(8));
        } else {
            $this->keys["data_final"] = "";
        }

        /* QUERY PARA PAGINACAO */
        $data = $this->model->getData("corrida", "a.id, a.time, a.motorista_id, a.cliente_id, a.comissao_motorista, a.comissao_carlog, a.inicio, a.fim, a.valor, mt.nome as motorista_nome, SUM(comissao_motorista) as valor_motorista, SUM(valor) as total, COUNT(a.id) as quantidade, COUNT(cr.id) as crecusadas", $filters, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id","a.motorista_id");

        $valores = $this->model->getData("corrida","SUM(comissao_carlog + comissao_motorista) as total, SUM(comissao_motorista) as total_motorista", $filters,"","a.id desc","inner join motorista as mt on a.motorista_id = mt.id");

        $filtersAceitas = array();
        $filtersAceitas = $filters;
        $filtersAceitas["a.status"] = "finalizada";
        $aceitas = $this->model->getData("corrida","COUNT(a.id) as aceitas", $filtersAceitas, $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id");
        $totAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

        $filtersCanceladas = array();
        $filtersCanceladas = $filters;
        $filtersCanceladas["a.status"] = "cancelada";
        $canceladas = $this->model->getData("corrida","COUNT(a.id) as canceladas", $filtersCanceladas,  $limits, "a.id ASC","inner join motorista as mt on a.motorista_id = mt.id left join corrida_recusada as cr on a.id = cr.corrida_id");
        $totRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

        $total = $this->model->countData("corrida", $filters);

        $parametrosAdicionais = $this->getParameter(4)."/".$this->getParameter(5)."/".$this->getParameter(6)."/".$this->getParameter(7)."/".$this->getParameter(8);
        $this->keys["paginacao"] = $this->ui->pager($steper, $total, $page, "goUrl", $parametrosAdicionais);

        // Fim da paginação
        if($data[0]["result"] == "empty") {
            $this->keys["displayTotais"] = "hidden";
            $this->keys["table"] = $this->includeHTML("../view/helpers/vazio.html");
            $this->keys["paginacao"] = "";
            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais(0);
            $this->keys["totalMotoristas"] = reais(0);
        } else {

            $lista[0][0] = "Motorista";
            $lista[0][1] = "Valor Motorista";
            $lista[0][2] = "Valor Total";
            $lista[0][3] = "Corridas";
            $lista[0][4] = "Chamadas aceitas";
            $lista[0][5] = "Chamadas recusadas";

            foreach ($data as $corrida) {

                $aceitas = $this->model->getData("corrida","COUNT(id) as aceitas",array("status"=>"finalizada","motorista_id"=>$corrida["motorista_id"]));
                $countAc = ( $aceitas[0]["result"] != "empty" ) ? $aceitas[0]["aceitas"] : 0;

                $canceladas = $this->model->getData("corrida","COUNT(id) as canceladas",array("status"=>"cancelada","motorista_id"=>$corrida["motorista_id"]));
                $countRec = ( $canceladas[0]["result"] != "empty" ) ? $canceladas[0]["canceladas"] : 0;

                $saida["motorista_nome"] = $corrida["motorista_nome"];
                $saida["valor_motorista"] = reais($corrida["valor_motorista"]);
                $saida["valor_total"] = reais($corrida["total"]);
                $saida["corridas"]  = $corrida["quantidade"];
                $saida["aceitas"]   = $countAc;
                $saida["recusadas"]   = $countRec;

                $lista[] = $saida;
            }

            $this->keys["aceitas"] = $totAc;
            $this->keys["recusadas"] = $totRec;
            $this->keys["totalCorridas"] = reais($valores[0]["total"]);
            $this->keys["totalMotoristas"] = reais($valores[0]["total_motorista"]);

            $this->keys["table"] = $this->html->table($lista,array("class"=>"table table-bordered table-hover table-striped upper text-center","id"=>"lista-corridas"),true,"","",true);
            $this->keys["paginacao"] = $this->ui->pager($steper,$total,$page,"goUrl");
        }
        return $this->keys;
    }

    public function _actionFiltrar_assinaturas() {

      $modulo = $this->getParameter('1');

      foreach ($_POST as $key => $valueTxt) {
        $key = str_replace('like_','like ',$key);
        if($valueTxt != '') {
          $_SESSION['filtros']['relatorio_assinaturas'][$key] = $valueTxt;
        }
        if($valueTxt == '') {
          unset($_SESSION['filtros']['relatorio_assinaturas'][$key]);
        }

        if($_SESSION['filtros']['relatorio_assinaturas'][$key] == '0') {
          unset($_SESSION['filtros']['relatorio_assinaturas'][$key]);
        }
      }

      $this->redirect("/relatorio/assinaturas");
    }
    public function _actionFiltrar_desistencias() {

      $modulo = $this->getParameter('1');

      foreach ($_POST as $key => $valueTxt) {
        $key = str_replace('like_','like ',$key);
        if($valueTxt != '') {
          $_SESSION['filtros']['relatorio_desistencias'][$key] = $valueTxt;
        }
        if($valueTxt == '') {
          unset($_SESSION['filtros']['relatorio_desistencias'][$key]);
        }

        if($_SESSION['filtros']['relatorio_desistencias'][$key] == '0') {
          unset($_SESSION['filtros']['relatorio_desistencias'][$key]);
        }
      }

      $this->redirect("/relatorio/desistencia");
    }


    public function _actionAssinaturas() {
      #dados do cliente
        $cliente = $this->model->getList("clientes","id","nome");
        $this->keys['select_cliente'] = $this->html->select(false, $cliente, 'cliente_id',$_SESSION['filtros']['relatorio_assinaturas']['cliente_id'],0);

        #dados do plano
        $plano = $this->model->getList("planos","id","nome");
        $this->keys['select_plano'] = $this->html->select(false, $plano, 'plano_id',$_SESSION['filtros']['relatorio_assinaturas']['plano_id'],0);

      $steper = 15;
      $modulo = $this->getParameter('1');
      $page = ($this->getParameter('3') != '') ? $this->getParameter('3') : 1;

      $total = $this->keys['total'] =  $this->model->countData('assinaturas',$_SESSION['filtros']['relatorio_assinaturas']);

      $this->keys['paginacao'] = $this->ui->pager($steper,$total,$page,'goUrl');

      $limits['limit'] = $steper;
      $limits['start'] = $this->calculaStartPaginacao($page,$steper);

      $filtros = $_SESSION['filtros']['relatorio_assinaturas'];
      $filtros['status'] = 'Pago';
      if($filtros['inicio'] != '') {
        $d = explode('/',$filtros['inicio']);

        $filtros['>= UNIX_TIMESTAMP(a.time)'] = mktime(0,0,0,$d[1],$d[0],$d[2]);
        $this->keys['filtro_inicio'] = $filtros['inicio'];
        unset($filtros['inicio']);
      }
      if($filtros['final'] != '') {
        $d = explode('/',$filtros['final']);

        $filtros['<= UNIX_TIMESTAMP(a.time)'] = mktime(0,0,0,$d[1],$d[0],$d[2]);
        $this->keys['filtro_final'] = $filtros['final'];
        unset($filtros['final']);
      }

      $dados = $this->model->getData('assinaturas','*,UNIX_TIMESTAMP(a.time) as data',$filtros,$limits);

      if($_SESSION['filtros']['relatorio_assinaturas'] != '') {
        $this->keys['limpar'] = '<a href="/relatorio/limpafiltros_relatorio_assinaturas" class="btn btn-info"><i class="glyphicon glyphicon-zoom-out" aria-hidden="true"></i> Limpar</a>';
        $this->keys['filtroativo'] = 'filtroativo';
      } else {
        $this->keys['limpar'] = '';
        $this->keys['filtroativo'] = '';
      }

      if($dados[0]['result'] != 'empty') {
          $tabela[0]['id'] = '#ID';
          $tabela[0]['Cliente'] = 'Cliente';
          $tabela[0]['Data'] = 'Data da assinatura';
         $tabela[0]['Plano'] = 'Plano';
         $tabela[0]['Status'] = 'Status';
         //$tabela[0]['Cartão'] = 'Cartão';
         //$tabela[0]['Nome titular'] = 'Nome titular';
         //$tabela[0]['Validade Cartão'] = 'Validade Cartão';

          //$tabela[0]['acoes'] = 'Ações';
          $x = 1;
          foreach($dados as $dado) {
            $tabela[$x]['id'] = $dado['id'];
            $tabela[$x]['cliente'] = $cliente[$dado['cliente_id']];
            $tabela[$x]['data'] = date('d/m/Y',$dado['data']);
             $tabela[$x]['plano'] = $plano[$dado['plano_id']];
             $tabela[$x]['status'] = $dado['status'];
            //  $tabela[$x]['cartao'] = $dado['cartao'];
            //  $tabela[$x]['nome_titular'] = $dado['nome_titular'];
            //  $tabela[$x]['validade_cartao'] = $dado['validade_cartao'];

            //$tabela[$x]['acoes'] = $this->html->link('Ver',"/assinaturas/ver/$dado[id]",'','btn btn-info btn-xs');

            $x++;
          }
          $this->keys['tabela'] = $this->html->table($tabela,array('class'=>'table table-bordered table-condensed table-hover table-striped upper tabela-listar ','id'=>'lista-assinaturas'),true,'','',true);
      } else {
          $this->keys['tabela'] = $this->html->div('Não foram encontrados assinaturas cadastrados  ',array('class'=>'center'));
      }

      #aplica filtros
      foreach($_SESSION['filtros'][$modulo] as $key => $value) {
        $key = str_replace('like','',$key);
        $this->keys['filtro_'.trim($key)] = $value;
      }
      return $this->keys;
   }
    public function _actionDesistencia() {
      #dados do cliente
        $cliente = $this->model->getList("clientes","id","nome");
        $this->keys['select_cliente'] = $this->html->select(false, $cliente, 'cliente_id',$_SESSION['filtros']['relatorio_desistencias']['cliente_id'],0);

        #dados do plano
        $plano = $this->model->getList("planos","id","nome");
        $this->keys['select_plano'] = $this->html->select(false, $plano, 'plano_id',$_SESSION['filtros']['relatorio_desistencias']['plano_id'],0);

      $steper = 15;
      $modulo = $this->getParameter('1');
      $page = ($this->getParameter('3') != '') ? $this->getParameter('3') : 1;

      $total = $this->keys['total'] =  $this->model->countData('assinaturas',$_SESSION['filtros']['relatorio_desistencias']);

      $this->keys['paginacao'] = $this->ui->pager($steper,$total,$page,'goUrl');

      $limits['limit'] = $steper;
      $limits['start'] = $this->calculaStartPaginacao($page,$steper);

      $filtros = $_SESSION['filtros']['relatorio_desistencias'];
      $filtros['dif status'] = 'Pago';
      if($filtros['inicio'] != '') {
        $d = explode('/',$filtros['inicio']);

        $filtros['>= UNIX_TIMESTAMP(a.time)'] = mktime(0,0,0,$d[1],$d[0],$d[2]);
        $this->keys['filtro_inicio'] = $filtros['inicio'];
        unset($filtros['inicio']);
      }
      if($filtros['final'] != '') {
        $d = explode('/',$filtros['final']);

        $filtros['<= UNIX_TIMESTAMP(a.time)'] = mktime(0,0,0,$d[1],$d[0],$d[2]);
        $this->keys['filtro_final'] = $filtros['final'];
        unset($filtros['final']);
      }

      $dados = $this->model->getData('assinaturas','*,UNIX_TIMESTAMP(a.time) as data',$filtros,$limits);

      if($_SESSION['filtros']['relatorio_desistencias'] != '') {
        $this->keys['limpar'] = '<a href="/relatorio/limpafiltros_relatorio_desistencias" class="btn btn-info"><i class="glyphicon glyphicon-zoom-out" aria-hidden="true"></i> Limpar</a>';
        $this->keys['filtroativo'] = 'filtroativo';
      } else {
        $this->keys['limpar'] = '';
        $this->keys['filtroativo'] = '';
      }

      if($dados[0]['result'] != 'empty') {
          $tabela[0]['id'] = '#ID';
          $tabela[0]['Cliente'] = 'Cliente';
          $tabela[0]['Data'] = 'Data da assinatura';
         $tabela[0]['Plano'] = 'Plano';
         $tabela[0]['Status'] = 'Status';
         //$tabela[0]['Cartão'] = 'Cartão';
         //$tabela[0]['Nome titular'] = 'Nome titular';
         //$tabela[0]['Validade Cartão'] = 'Validade Cartão';

          //$tabela[0]['acoes'] = 'Ações';
          $x = 1;
          foreach($dados as $dado) {
            $tabela[$x]['id'] = $dado['id'];
            $tabela[$x]['cliente'] = $cliente[$dado['cliente_id']];
            $tabela[$x]['data'] = date('d/m/Y',$dado['data']);
             $tabela[$x]['plano'] = $plano[$dado['plano_id']];
             $tabela[$x]['status'] = $dado['status'];
            //  $tabela[$x]['cartao'] = $dado['cartao'];
            //  $tabela[$x]['nome_titular'] = $dado['nome_titular'];
            //  $tabela[$x]['validade_cartao'] = $dado['validade_cartao'];

            //$tabela[$x]['acoes'] = $this->html->link('Ver',"/assinaturas/ver/$dado[id]",'','btn btn-info btn-xs');

            $x++;
          }
          $this->keys['tabela'] = $this->html->table($tabela,array('class'=>'table table-bordered table-condensed table-hover table-striped upper tabela-listar ','id'=>'lista-assinaturas'),true,'','',true);
      } else {
          $this->keys['tabela'] = $this->html->div('Não foram encontrados assinaturas cadastrados  ',array('class'=>'center'));
      }

      #aplica filtros
      foreach($_SESSION['filtros'][$modulo] as $key => $value) {
        $key = str_replace('like','',$key);
        $this->keys['filtro_'.trim($key)] = $value;
      }
      return $this->keys;
   }
   public function _actionLimpafiltros_relatorio_assinaturas() {
     $modulo = $this->getParameter('1');
     unset($_SESSION['filtros']['relatorio_assinaturas']);
     $this->redirect("/relatorio/assinaturas");
   }
   public function _actionLimpafiltros_relatorio_desistencias() {
     $modulo = $this->getParameter('1');
     unset($_SESSION['filtros']['relatorio_desistencias']);
     $this->redirect("/relatorio/desistencia");
   }
}

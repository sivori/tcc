angular.module('app.controllers')
  .controller('meuPerfilCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {
      SIMPLEJS.mask("telefone", "#telefone");
      $scope.usuario = JSON.parse(SIMPLEJS.lsget("usuario"));

      $scope.salvar = function(obj) {
        if (obj.senha != undefined) {
        	if (obj.senha != obj.confirma){
              $ionicPopup.alert({
                  title:'Oops...',
                  template: 'As senhas não conferem!',
                  okType: 'calm'
              })
  				    return false;
    			}
        }
        
        obj.id = SIMPLEJS.lsget("id");

        SIMPLEJS.actionApi("AlteraPerfil", obj, true).then(function(data) {
          if (data.data.status == 'success') {
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Alterado!",
              template: data.data.mensagem,
              okType: "energized"
            })
            .then(function(res){
              SIMPLEJS.lsadd("usuario", JSON.stringify(data.data.data));
              $state.go('tabsController.home');
            })
          } else {
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Ops!",
              template: data.data.mensagem,
              okType: "calm"
            });
          }
        }).catch(function(error) {
          console.error(error);
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }
    }
  ]);

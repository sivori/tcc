angular.module('app.controllers')
  .controller('sairCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {

      $scope.sair = function() {
        window.localStorage.clear();
        $state.go('login');
      };
    }
  ]);

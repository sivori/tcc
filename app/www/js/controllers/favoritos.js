angular.module('app.controllers')
  .controller('favoritosCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {
      $scope.usuario = JSON.parse(SIMPLEJS.lsget('usuario'));

      $scope.buscaEstabelecimentos = function() {
        SIMPLEJS.lsrm('estabelecimento');

        let dado_id = {
          id: SIMPLEJS.lsget('id')
        }

        SIMPLEJS.actionApi("BuscaEstabelecimentosFavoritos", dado_id, true).then(function(data) {
          console.log(data.data)
          if (data.data.status == 'sucesso') {
            $scope.listaEstabelecimentos = data.data.data;
          } else {
            $scope.listaEstabelecimentos = null;
          }
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      };

      $scope.removeFavorito = function(favorito) {
        let dados = {
          estabelecimento_id: favorito.estabelecimento_id,
          cliente_id: $scope.usuario.id
        }

        SIMPLEJS.actionApi("RemoveFavorito", dados, true).then(function(data) {
          $scope.buscaEstabelecimentos();
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }

      $scope.buscaEstabelecimentos();

      $scope.goToestabelecimentoServicos = function(estabelecimento) {
        SIMPLEJS.lsadd("estabelecimento", JSON.stringify(estabelecimento));
        $state.go('tabsController.estabelecimentoServico', { obj: 'favoritos' });
      }

    }
  ]);

angular.module('app.controllers')
  .filter('split', function() {
    return function(input, delimiter) {
      var delimiter = delimiter || ',';

      return input.split(delimiter);
    }
  })

  .controller('estabelecimentoServicoCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {
      $scope.telaAnterior = $stateParams.obj
      $scope.informacoes = true;
      $scope.endereco = false;
      $scope.estabelecimento = JSON.parse(SIMPLEJS.lsget("estabelecimento"));
      $scope.usuario = JSON.parse(SIMPLEJS.lsget('usuario'));

      $scope.pageInformacoes = function() {
        $scope.informacoes = true;
        $scope.endereco = false;
      }

      $scope.pageEndereco = function() {
        $scope.informacoes = false;
        $scope.endereco = true;
      }

      $scope.cadastraFavorito = function() {
        let dados = {
          estabelecimento_id: $scope.estabelecimento.id,
          cliente_id: $scope.usuario.id
        }

        SIMPLEJS.actionApi("CadastraFavorito", dados, true).then(function(data) {
          $scope.estabelecimento.favoritos = "1";
          SIMPLEJS.lsadd("estabelecimento", JSON.stringify($scope.estabelecimento));
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }

      $scope.removeFavorito = function() {
        let dados = {
          estabelecimento_id: $scope.estabelecimento.id,
          cliente_id: $scope.usuario.id
        }

        SIMPLEJS.actionApi("RemoveFavorito", dados, true).then(function(data) {
          $scope.estabelecimento.favoritos = "0";
          SIMPLEJS.lsadd("estabelecimento", JSON.stringify($scope.estabelecimento));
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }
    }
  ]);

angular.module('app.controllers')
  .controller('esqueciMinhaSenhaCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet) {

      $scope.enviar = function(obj) {
        console.log(obj);
        if (SIMPLEJS.validateEmpty("#esqueciMinhaSenha", true)) {
          if (obj != null) {
            SIMPLEJS.actionApi("EsqueciSenha", obj, true).then(function(data) {
              console.log(data.data)
              if (data.status == 'sucesso') {
                $ionicPopup.alert({
                  title: "Enviado !",
                  template: data.mensagem
                }).then(function(res) {
                  $state.go('login')
                })
              } else {
                $ionicPopup.alert({
                  title: "Ops!",
                  template: data.mensagem
                })
              }
            }).error(function(error) {
              console.error(error)
              $ionicPopup.alert({
                title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
              });
            })
          } else {
            $ionicPopup.alert({
              title: "Digite um e-mail!",
              template: ('O campo e-mail esta vazio.')
            })
          }
        }
        $scope.cancelar = function(objeto) {
          if (objeto != null) {
            delete objeto.email
            $state.go('login')
          } else {
            $state.go('login')
          }
        }
      }
    }
  ]);

angular.module('app.controllers')
  .controller('cadastroCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$http', '$ionicActionSheet','$ionicModal',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $http, $ionicActionSheet,$ionicModal) {
      SIMPLEJS.mask("telefone", "#telefone");

      $scope.salvar = function(usuario) {
        if (usuario == undefined || usuario.nome == undefined || usuario.email == undefined || usuario.senha == undefined) {
          $ionicPopup.alert({
            title:'Oops...',
            template: 'Preencha os campos corretamente!',
            okType: 'energized'
          })
          return false;
        }

        if (usuario.senha != usuario.confirma){
          $ionicPopup.alert({
            title:'Oops...',
            template: 'As senhas não conferem!',
            okType: 'energized'
          })
          return false;
        }

        SIMPLEJS.actionApi("cadastro", usuario, true).then(function(data) {
          if (data.data.status == 'success') {
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Cadastrado!",
              template: data.data.mensagem,
              okType: "energized"
            })
            .then(function(res){
              console.log(data.data.data.id)
              SIMPLEJS.lsadd("usuario", JSON.stringify(data.data));
              SIMPLEJS.lsadd("id", data.data.data.id);
              SIMPLEJS.lsadd("logado", true);
              // $state.go('tabsController.home');
            })
          } else {
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Ops!",
              template: data.data.mensagem,
              okType: "energized"
            });
          }
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }
    }
  ]);

angular.module('app.controllers')
  .controller('agendarVisitaCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {
      $scope.estabelecimento = JSON.parse(SIMPLEJS.lsget("estabelecimento"));
      $scope.usuarioId = SIMPLEJS.lsget("id");
      $scope.profissionais = {};
      $scope.profissionalEscolhido = {};
      $scope.datas = [];
      $scope.diaEscolhido = "";

      $scope.buscarProfissionais = function() {
        SIMPLEJS.actionApi("BuscarProfissionais", { estabelecimento_id: $scope.estabelecimento.id }, true).then(function(data) {
          if (data.data.status == 'sucesso') {
            $scope.profissionais = data.data.data;
          }
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }

      $scope.changeProfissional = function() {
        $scope.diaEscolhido = "";

        if($scope.profissionalEscolhido.dados.length != {}) {
          $scope.datasAgendamento();
        }
      }
      
      $scope.datasAgendamento = function() {
        SIMPLEJS.actionApi("BuscaHorariosAgendados", { profissional: $scope.profissionalEscolhido.dados, estabelecimento: $scope.estabelecimento }, true).then(function(result) {
          if (result.data.status == 'sucesso') {
            $scope.datas = result.data.data;
          }
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }


      $scope.formataDia = function(dia) {
        var day = dia;

        if(day.toString().length == 1) {
          day = "0" + day;
        }

        return day;
      }

      $scope.buttonData = function(id) {
        if ($("#hs" + id)[0].style.display == "none") {
          var count = 0;

          while($("#hs" + count)[0] != undefined) {
            $("#hs" + count)[0].style.display = "none";

            count++;
          }

          $("#hs" + id)[0].style.display = "";
        } else{
          $("#hs" + id)[0].style.display = "none";
        }
      }
      
      $scope.escolheData = function(element) {
        if(element.target.class == "button button-calm button-outline btnHora") {
          element.target.class = "button button-stable btnHora";
          element.target.style.background = "#fff";
          element.target.style.color = "#4c171d";

          $scope.diaEscolhido = "";
        } else{
          var elements = $(".btnHora");

          for(var i = 0; i < elements.length; i++) {
            elements[i].class = "button button-stable btnHora";
            elements[i].style.background = "#fff";
            elements[i].style.color = "#4c171d";
          }

          element.target.class = "button button-calm button-outline btnHora";
          element.target.style.background = "#4c171d";
          element.target.style.color = "#fff";

          $scope.diaEscolhido = element.target.parentElement.parentElement.parentElement.parentElement.children[0].children[0].innerHTML + " - " + element.target.innerHTML;
        }
      }

      $scope.confirmarAgendamento = function() {
        var dados = {
          usuarioId: $scope.usuarioId,
          profissionalId: $scope.profissionalEscolhido.dados.id,
          data: $scope.diaEscolhido
        }

        SIMPLEJS.actionApi("ConfirmarAgendamento", dados, true).then(function(data) {
          if (data.data.status == 'sucesso') {
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Sucesso!",
              subTitle: data.data.mensagem,
              okType: "light"
            }).then(function() {
              $scope.profissionais = {};
              $scope.profissionalEscolhido = {};
              $scope.datas = [];
              $scope.diaEscolhido = "";

              $state.go("tabsController.home");
            });
          } else{
            $ionicPopup.alert({
              cssClass: "animated bounceInUp",
              title: "Ops!",
              subTitle: data.data.mensagem,
              okType: "light"
            });
          }
        }).catch(function(error) {
          $ionicPopup.alert({
            title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
          });
        });
      }

      $scope.buscarProfissionais();
    }
  ]);
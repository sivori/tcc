angular.module('app.controllers')
  .controller('loginCtrl', ['$ionicPlatform', '$scope', '$stateParams', 'SIMPLEJS', '$ionicLoading', '$ionicPopup', '$state', '$rootScope', '$ionicHistory', '$ionicActionSheet', '$ionicSideMenuDelegate',
    function($ionicPlatform, $scope, $stateParams, SIMPLEJS, $ionicLoading, $ionicPopup, $state, $rootScope, $ionicHistory, $ionicActionSheet, $ionicSideMenuDelegate) {

      $ionicSideMenuDelegate.canDragContent(false);

      $scope.logado = function() {
        if (SIMPLEJS.lsget("logado")) {
          $state.go('tabsController.home');
        }
      }

      $scope.login = function(obj) {
        //VERIFICA SE FORMULARIO FOI PREENCHIDO E NÃO ESTA LOGANDO ATRAVES DE REDES SOCIAIS
        if (SIMPLEJS.validateEmpty("#login", true)) {
          // if (SIMPLEJS.validateEmpty("#login", true) || social) {
          SIMPLEJS.actionApi("login", obj, true).then(function(data) {
            if (data.data.status == 'success') {
              SIMPLEJS.lsadd("usuario", JSON.stringify(data.data.data));
              SIMPLEJS.lsadd("id", data.data.data.id);
              $rootScope.usuario = JSON.parse(SIMPLEJS.lsget('usuario'));
              $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
              });
              $rootScope.logado = true;
              SIMPLEJS.lsadd("logado", true);
              $state.go('tabsController.home');
              // $rootScope.gravaaparelho()
              // $rootScope.toast("Seja bem-vindo " + data.data.nome);
              // $rootScope.gravaaparelho()

            } else if (data.status == 'cadastro') {
              $ionicPopup.confirm({
                cssClass: "animated bounceInUp",
                title: "Ops!",
                template: data.mensagem,
                okText: "Cadastrar",
                cancelText: "Fechar"
              }).then(function(res) {
                if (res) {
                  $state.go("cadastreSe", {
                    email: obj.email
                  })
                }
              })
            } else {
              $ionicPopup.alert({
                cssClass: "animated bounceInUp",
                title: "Ops!",
                subTitle: data.data.mensagem,
                okType: "light"
              });
            }
          }).catch(function(error) {
            console.error(error);
            $ionicPopup.alert({
                title: ":( Ocorreu uma falha de comunicação, tente novamente mais tarde ou verifique sua conexão com a internet"
            });
          });
          // }
        }
      }

      $scope.logado();
    }
  ]);

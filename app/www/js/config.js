angular.module('app.config', [])
  .constant('LOCALSTORAGE', 'hinchliffe')



  // MAC
  // .constant('API', 'http://hinchliffe.mac/api/')
  // .constant('URL', 'http://hinchliffe.mac/')

  //LOCAL
  .constant('API', 'http://hinchliffe.local/api/')
  .constant('URL', 'http://hinchliffe.local/')

  // .constant('API', 'http://hinchliffe.biel/api/')
  // .constant('URL', 'http://hinchliffe.biel/')

  // // DEV
  // .constant('API', 'http://hinchliffe.alphacode.mobi/api/')
  // .constant('URL', 'http://hinchliffe.alphacode.mobi/')

  // // PROD
  // .constant('URL', 'http://hinchliffe.com.br/')
  //     .constant('API', 'http://hinchliffe.com.br/api/')
  .constant('APP_DOWNLOAD', 'http://appfabioteruel.com.br');

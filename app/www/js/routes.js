angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    url: '/login',
    cache: false,
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('esqueciMinhaSenha', {
    url: '/esqueci-minha-senha',
    templateUrl: 'templates/esqueciMinhaSenha.html',
    controller: 'esqueciMinhaSenhaCtrl'
  })

  .state('cadastro', {
    url: '/cadastro',
    templateUrl: 'templates/cadastro.html',
    controller: 'cadastroCtrl'
  })

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  /*
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.home'
      2) Using $state.go programatically:
        $state.go('tabsController.home');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/home
      /page1/tab5/home
  */
  .state('tabsController.home', {
    url: '/home',
    cache: false,
    views: {
      'tab1': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  /*
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.filtro'
      2) Using $state.go programatically:
        $state.go('tabsController.filtro');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/filtro
      /page1/tab5/filtro
  */

  .state('filtro', {
    url: '/filtro',
    templateUrl: 'templates/filtro.html',
    controller: 'filtroCtrl'
  })

  .state('tabsController.filtro', {
    url: '/filtro',
    views: {
      'tab1': {
        templateUrl: 'templates/filtro.html',
        controller: 'filtroCtrl'
      }
    }
  })

  /*
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.estabelecimentoServico'
      2) Using $state.go programatically:
        $state.go('tabsController.estabelecimentoServico');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/estabelecimento-servico
      /page1/tab5/estabelecimento-servico
  */
  .state('tabsController.estabelecimentoServico', {
    url: '/estabelecimento-servico',
    params: {
      obj: null
    },
    views: {
      'tab1': {
        templateUrl: 'templates/estabelecimentoServico.html',
        controller: 'estabelecimentoServicoCtrl',
      },
      'tab3': {
        templateUrl: 'templates/estabelecimentoServico.html',
        controller: 'estabelecimentoServicoCtrl'
      }
    }
  })

  .state('tabsController.agendarVisita', {
    url: '/agendar-visita',
    views:{
      'tab1': {
        templateUrl: 'templates/agendarVisita.html',
        controller: 'agendarVisitaCtrl'
      },
      'tab3': {
        templateUrl: 'templates/agendarVisita.html',
        controller: 'agendarVisitaCtrl'
      }
    }
  })

  .state('agendamentoConcluido', {
    url: '/agendamento-concluido',
    templateUrl: 'templates/agendamentoConcluido.html',
    controller: 'agendamentoConcluidoCtrl'
  })

  .state('avaliado', {
    url: '/avaliado',
    templateUrl: 'templates/avaliado.html',
    controller: 'avaliadoCtrl'
  })

  .state('tabsController.favoritos', {
    url: '/favoritos',
    cache: false,
    views: {
      'tab3': {
        templateUrl: 'templates/favoritos.html',
        controller: 'favoritosCtrl'
      }
    }
  })

  .state('tabsController.minhaAgenda', {
    url: '/minha-agenda',
    views: {
      'tab2': {
        templateUrl: 'templates/minhaAgenda.html',
        controller: 'minhaAgendaCtrl'
      }
    }
  })

  .state('historico', {
    url: '/historico',
    templateUrl: 'templates/historico.html',
    controller: 'historicoCtrl'
  })

  .state('tabsController.meuPerfil', {
    url: '/MeuPerfil',
    views: {
      'tab4': {
        templateUrl: 'templates/meuPerfil.html',
        controller: 'meuPerfilCtrl'
      }
    }
  })

  .state('tabsController.verAgenda', {
    url: '/ver-agenda',
    views: {
      'tab2': {
        templateUrl: 'templates/verAgenda.html',
        controller: 'verAgendaCtrl'
      }
    }
  })

  .state('verHistorico', {
    url: '/ver-historico',
    templateUrl: 'templates/verHistorico.html',
    controller: 'verHistoricoCtrl'
  })

  .state('tabsController.sair', {
    url: '/sair',
    views: {
      'tab5': {
        templateUrl: 'templates/sair.html',
        controller: 'sairCtrl'
      }
    }
  })

  .state('avaliar', {
    url: '/avaliar',
    templateUrl: 'templates/avaliar.html',
    controller: 'avaliarCtrl'
  })

$urlRouterProvider.otherwise('/login')


});

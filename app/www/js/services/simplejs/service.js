angular.module('app.services', ['app.config'])
  .service('SIMPLEJS', function(API, $ionicPopup, $ionicLoading, $http, LOCALSTORAGE, $rootScope) {
    SIMPLEJS_PATH = document.getElementsByTagName("path")[0].attributes[0].nodeValue
    var linkElem = document.createElement('link');
    document.getElementsByTagName('head')[0].appendChild(linkElem);
    linkElem.rel = 'stylesheet';
    linkElem.type = 'text/css';
    linkElem.href = SIMPLEJS_PATH + 'libs/animate.css';

    // REMOVE TODOS OS ESPACOES DE UMA STRING
    this.trim = function(string) {
      return string.replace(/^\s+|\s+$/g, "")
    }

    //ANIMAÇÃO DE COMPONENTES
    //OBRIGATORIO animate.css
    //https://github.com/daneden/animate.css
    this.anima = function(seletor, efeito) {
      $(seletor).addClass('animated ' + efeito)
      setTimeout(function() {
        $(seletor).removeClass('animated ' + efeito)
      }, 3000)
    }

    // AUTOCOMPLETE
    this.autocomplete = function(seletor, path, minLength) {
      console.log("aa");
      $(seletor).autocomplete({
        source: function(request, response) {

          $.ajax({
            url: API + path,
            type: 'POST',
            dataType: 'json',
            minLength: minLength,
            data: {
              termo: $(seletor).val()
            },
            success: function(json) {
              console.log(json);
              response($.map(json, function(item) {
                return {
                  label: item
                }
              }))
            }
          })
        },
        select: function(e, ui) {
          $(seletor).val(ui.item.value)
        }
      })
    }


    // LOCALSTORAGE
    this.lsadd = function(chave, valor) {
      window.localStorage.setItem(LOCALSTORAGE + "." + chave, valor)
    }
    this.lsget = function(chave) {
      return window.localStorage.getItem(LOCALSTORAGE + "." + chave)
    }
    this.lsrm = function(chave) {
      window.localStorage.removeItem(LOCALSTORAGE + "." + chave)
    }

    this.validateEmpty = function(seletor, animacao) {

      // INPUTS
      var inputsVazios = $(seletor + " .required").filter(function() {
        if (this.value.replace(/^\s+|\s+$/g, "") == "") {
          return !this.value
        }
      }).get()
      // SELECTS

      var selectsVazio = $(seletor + " select.required option:selected").filter(function() {

        if (this.text.trim() == '') {
          console.log(this.text)
          return !this.text
        }
      }).get()


      var inputspreenchidos = $(seletor + " input.required").filter(function() {
        return this.value
      }).get()

      var selectsPreenchidos = $(seletor + " select.required option:selected").filter(function() {
        return this.text
      }).get()
      // console.log(vazios)
      var vazios = inputsVazios.concat(selectsVazio)
      console.log(vazios);
      var preenchidos = selectsPreenchidos.concat(inputspreenchidos)

      // VALIDA CPF
      if ($(seletor + " .valida_cpf").val()) {
        cpf = $(seletor + " .valida_cpf").val()
        cpf = cpf.replace('.', '');
        cpf = cpf.replace('-', '');
        cpf = cpf.replace('.', '');

        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11) {
          $(seletor + " .valida_cpf").parent().addClass('vazio')
          $rootScope.toast("CPF inválido")
          return false;
        }

        for (i = 0; i < cpf.length - 1; i++) {
          if (cpf.charAt(i) != cpf.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
          }
        }

        if (!digitos_iguais) {
          numeros = cpf.substring(0, 9);
          digitos = cpf.substring(9);
          soma = 0;
          for (i = 10; i > 1; i--) {
            soma += numeros.charAt(10 - i) * i;
          }
          resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
          if (resultado != digitos.charAt(0)) {
            $(seletor + " .valida_cpf").parent().addClass('vazio')
            $rootScope.toast("CPF inválido")

            return false;
          }
          numeros = cpf.substring(0, 10);
          soma = 0;
          for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
          resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
          if (resultado != digitos.charAt(1)) {
            console.log("erro");
            $(seletor + " .valida_cpf").parent().addClass('vazio')
            $rootScope.toast("CPF inválido")
            return false;

          }

        } else {
          $(seletor + " .valida_cpf").parent().addClass('vazio')
          $rootScope.toast("CPF inváido")
          return false;
        }

      }

      if (vazios.length) {
        $("*").removeClass('select_vazio')
        $("*").removeClass('vazio')

        $(vazios).parent().addClass('vazio')
        $("select.vazio").parent().addClass('select_vazio')

        if (animacao) {
          $(seletor).addClass('animated shake')
          setTimeout(function() {
            $(seletor).removeClass('animated shake')
          }, 3000)
        }
        $rootScope.toast("Preencha todos os campos obrigatórios")
        $(vazios[0]).focus()
        return false
      } else {
        $(preenchidos).parent().removeClass('vazio')
        return true
      }

    }

    // path  = caminho da api recebe uma string
    // obj   = recebe um objeto, e envia para api como parametro get
    // after = executa uma ação apos executar o metodo aceita
    //   'popup' ou 'toast'
    // loading = executa o $ionicLoading recebe 'true' ou 'false'

    // actionApi - inicio
    this.actionApi = function(path, obj, loading) {
      console.log(loading);
      if (loading) {
        $ionicLoading.show({
          template: '<img src="img/spinner.svg" width="120px"/><br><h3>Aguarde...</h3>',
        });
      }

      return $http.get(API + path + "/", {
        params: obj
      }).success(function(data) {
        if (loading) {
          $ionicLoading.hide()
        }
      }).error(function(data, status) {
        console.group("ERRO ACTION API")
        console.log("data")
        console.log(">>>>>>>>>")
        console.error(data)
        console.log("status")
        console.error(status)
        console.log(">>>>>>>>>")
        console.groupEnd()
        if (loading) {
          $ionicLoading.hide()
        }
      })
    }


    // actionApi - fim

    // UPLOAD DE AVATAR - INICIO
    // PLUGINS
    // cordova plugin add cordova-plugin-camera && cordova plugin add cordova-plugin-file && cordova plugin add cordova-plugin-file-transfer 

    // mascaras
    this.mask = function(tipo, seletor) {
      $.getScript(SIMPLEJS_PATH + 'libs/mask.js', function() {
        if (tipo == 'valor') {
          $(seletor).mask('000000000000000,00', {
            reverse: true
            // placeholder: 'R$'
          });
        }
        if (tipo == 'altura') {
          $(seletor).mask('9.99')
        }
        if (tipo == 'cpf') {
          $(seletor).mask('999.999.999-99')
        }
        if (tipo == 'cpfcnpj') {
          var telMaskBehavior = function(val) {
              return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-00999'
            },
            spOptions = {
              onKeyPress: function(val, e, field, options) {
                field.mask(telMaskBehavior.apply({}, arguments), options)
              }
            }
          $(seletor).mask(telMaskBehavior, spOptions)
        }
        if (tipo == 'rg') {
          $(seletor).mask('99.999.999-9')
        }
        if (tipo == 'horario') {
          $(seletor).mask('99:99')
        }
        if (tipo == 'data') {
          $(seletor).mask('99/99/99')
        }
        if (tipo == 'data_full') {
          $(seletor).mask('99/99/9999')
        }
        if (tipo == 'data_card') {
          $(seletor).mask('99/99')
        }
        if (tipo == 'cep') {
          $(seletor).mask('99999-999')
        }
        if (tipo == 'cnpj') {
          $(seletor).mask('99.999.999/9999-99')
        }
        if (tipo == 'telefone') {
          $(seletor).mask('(00)00000-0000')
        }
        if (tipo == 'cartao') {
          $(seletor).mask('0000-0000-0000-0000')
        }
        if (tipo == 'cartao_validade') {
          $(seletor).mask('99/99')
        }
        if (tipo == 'celular') {
          $(seletor).mask('+00(00)00000-0000')
        }
        if (tipo == 'teleCel') {
          var telMaskBehavior = function(val) {
              return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009'
            },
            spOptions = {
              onKeyPress: function(val, e, field, options) {
                field.mask(telMaskBehavior.apply({}, arguments), options)
              }
            }
          $(seletor).mask(telMaskBehavior, spOptions)
        }
      })
    }

    this.uploadImagemVideo = function(tipo, dimensao) {
      return new Promise(function(resolve, reject) {
        var options = {
          quality: 70
        };
        //envia o avatar para o servidor depois do Crop

        function funcuploadMidia(fileURL) {
          var win = function(r) {
            console.log(r)
            // $rootScope.arquivos[tumb - 1] = API + "fotos/" + $.parseJSON(r.response).data
            console.log($rootScope.tumb1)
            console.log('>>>>>>>>>>>>>')
            console.log($rootScope.arquivos)
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
            $rootScope.$apply()
            $ionicLoading.hide()
            $rootScope.toast("Imagem enviada")
            resolve($.parseJSON(r.response).data)
          }

          var fail = function(error) {
            reject(error)
            $rootScope.toast("Falha no upload")
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
            $ionicLoading.hide()
          }

          //envia para o servidor
          var options = new FileUploadOptions();
          options.fileKey = "file";
          options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
          options.mimeType = "text/plain";

          var params = {};
          params.user_id = window.localStorage.getItem(LOCALSTORAGE + ".id")
          options.params = params;

          var ft = new FileTransfer();
          ft.upload(fileURL, encodeURI(API + "uploadArquivo"), win, fail, options);
          $ionicLoading.hide()

        }

        //retorno da captura inicial da imagem
        function cameraAvatarSuccess(imageData) {
          //de acordo com a plataforma chama o crop adequado
          console.log("==>" + imageData)
          $rootScope.imagemUpando = imageData;
          funcuploadMidia($rootScope.imagemUpando);

        }

        function cameraAvatarError(data) {
          console.error(data);
          // $rootScope.toast('Deu erro');
          $ionicLoading.hide()
        }

        if (tipo == 'camera') {
          //captura a imagem
          var cameraOptions = {
            quality: 30,
            sourceType: Camera.PictureSourceType.CAMERA,
            //destinationType: Camera.DestinationType.DATA_URL,
            destinationType: Camera.DestinationType.FILE_URI,
            EncodingType: 'PNG',
            correctOrientation: true
          };
          if (dimensao) {
            if (dimensao.width) {
              cameraOptions.targetWidth = dimensao.width
            }
            if (dimensao.heigth) {
              cameraOptions.targetHeight = dimensao.heigth
            }
          }

        } else {
          //captura a imagem
          var cameraOptions = {
            quality: 30,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            //destinationType: Camera.DestinationType.DATA_URL,
            destinationType: Camera.DestinationType.FILE_URI,
            EncodingType: 'PNG',
            allowEdit: true,
            correctOrientation: true
          };
          if (dimensao) {
            cameraOptions.targetWidth = dimensao.width

            if (dimensao.width) {
              cameraOptions.targetWidth = dimensao.width
            }
            if (dimensao.heigth) {
              cameraOptions.targetHeight = dimensao.heigth
            }
          }
        }
        navigator.camera.getPicture(cameraAvatarSuccess, cameraAvatarError, cameraOptions);
      });
    }

    this.getCEP = function(cep) {
      $ionicLoading.show({
        template: '<img src="img/spinner.svg" width="120px"/><br><h3>Aguarde...</h3>',
      });
      return $http.get("http://cep.alphacode.com.br/action/cep/" + cep).success(function(data) {
        console.log(data);
        if (data.error) {
          $ionicPopup.alert({
            cssClass: "animated bounceInUp",
            title: 'Ops!',
            template: data.error
          })
        }
        $ionicLoading.hide()
      }).error(function(data, status) {
        console.group("ERRO ACTION CEP")
        console.log("data")
        console.log(">>>>>>>>>")
        console.error(data)
        console.log("status")
        console.error(status)
        console.log(">>>>>>>>>")
        console.groupEnd()
        $ionicLoading.hide()
      })
    }
  })
